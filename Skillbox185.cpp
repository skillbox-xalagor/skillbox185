#include <iostream>

template <class T>
class Stack
{
private:
    //��������� ��������� ������������ ������ ��� ������ � ����� ����� ������
    struct StackNode
    {
        T Value;
        StackNode* Next;
    };

    StackNode* Top;

public:
    Stack() : Top(0)
    {}
    ~Stack();
    void Push(T);
    auto Pop();
    auto Peek();
    bool IsEmpty();
};

template <class T>
Stack<T>::~Stack()
{
    StackNode* NodePtr, * NextNode;

    NodePtr = Top;

    while (NodePtr != 0)
    {
        NextNode = NodePtr->Next;
        delete NodePtr;
        NodePtr = NextNode;
    }
}

template <class T>
void Stack<T>::Push(T Num)
{
    StackNode* NewNode;

    NewNode = new StackNode;
    NewNode->Value = Num;

    if (IsEmpty())
    {
        Top = NewNode;
        NewNode->Next = 0;
    }
    else
    {
        NewNode->Next = Top;
        Top = NewNode;
    }
}

template <class T>
auto Stack<T>::Pop()
{
    StackNode* Temp;

    if (IsEmpty())
    {
        std::cout << "The stack is empty.\n"; //�� ����, ��� ������� ������ ��� ���������� �������� � ���� �����
    }
    else
    {
        auto Value = Top->Value;
        Temp = Top->Next;
        delete Top;
        Top = Temp;
        return Value;
    }
}

template <class T>
auto Stack<T>::Peek()
{
    return Top->Value;
}

//���������� true ���� ���� ������
template <class T>
bool Stack<T>::IsEmpty()
{
    return (!Top);
}

int main() {

    Stack<int> StackExample;
    StackExample.Pop(); //�������������� ���������� � ��������� �� ������ ��-�� ������� �����
    StackExample.Push(12);
    std::cout << "Top element: " << StackExample.Peek() << '\n';
    return 0;
}